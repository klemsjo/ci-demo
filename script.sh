#! /bin/bash

if [[ -z "${TESTCASE_42}" ]]; then
  echo "Test cases 1-5" > output.txt
else
  echo "Test case 42"  > output.txt
fi